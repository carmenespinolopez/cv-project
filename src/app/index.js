import { onLoadPersonalInfo } from './scripts/personalInfo';
import { onLoadExperience, onlyShowWorldsensing, onlyShowVodafone, onlyShowFnac, onlyShowGestmusic } from './scripts/experience';
import { onlyShowMainPage, onlyShowPersonalPage, onlyShowKnowledgelPage, onlyShowExperiencePage, nextSectionMainPage, nextSectionPersonalPage, nextSectionKnowledgePage, nextSectionExperiencePage, onlyShowCurriculumPage, GoToPersonal } from './scripts/buttons';
import { onLoadKnowledge } from './scripts/studies';
import './styles/styles.scss';

var addListeners = () =>{
    document.getElementById("main-page-button").addEventListener("click", onlyShowMainPage);
    document.getElementById("personal-page-button").addEventListener("click", onlyShowPersonalPage);
    document.getElementById("knowledge-button").addEventListener("click", onlyShowKnowledgelPage);
    document.getElementById("experience-button").addEventListener("click", onlyShowExperiencePage);
    document.getElementById("curriculum-button").addEventListener("click", onlyShowCurriculumPage);
    document.getElementById("next-section-main-page").addEventListener("click", nextSectionMainPage);
    document.getElementById("next-section-personal-page").addEventListener("click", nextSectionPersonalPage);
    document.getElementById("next-section-knowledge-page").addEventListener("click", nextSectionKnowledgePage);
    document.getElementById("next-section-experience-page").addEventListener("click", nextSectionExperiencePage);
    document.getElementById("ws").addEventListener("click", onlyShowWorldsensing);
    document.getElementById("vf").addEventListener("click", onlyShowVodafone);
    document.getElementById("fnac").addEventListener("click", onlyShowFnac);
    document.getElementById("gm").addEventListener("click", onlyShowGestmusic);
    document.getElementById("go-to-personal").addEventListener("click", GoToPersonal);
}   

window.onload = () => {
    //Init Content
    onLoadPersonalInfo();
    onLoadExperience();
    onLoadKnowledge();

    //Event binding
    addListeners();
};


window.onscroll = function() {handleOnScroll()};
var header = document.getElementById("header");
var sticky = header.offsetTop;
function handleOnScroll() {
  console.log('hello');
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}