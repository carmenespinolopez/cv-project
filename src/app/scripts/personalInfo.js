var personalInfo = {
  name: "Carmen Espino López",
  address: "Esplugues de Llobregat, Barcelona.",
};

var contact = [
  {
    icon: "/assets/gmail.png",
    url: "mailto:carmenespinolopez@gmail.com",
    info: "email",
  },
  {
    icon: "/assets/linkedin.png",
    url: "https://www.linkedin.com/in/carmen-espino-lopez/",
    info: "linkedin",
  },
  {
    icon: "/assets/phone.png",
    url: "tel:655666018",
    info: "phone",
  },
  {
    icon: "/assets/gitlab.png",
    url: "https://gitlab.com/carmenespinolopez",
    info: "gitlab",
  },
];

var cosasGustan = [
  "Leer",
  "Escribir fantasía",
  "Jugar con mi gata",
  "Jugar a rol",
];
var cosasOdio = [
  "Las lentejas",
  "Los petados",
  "El chocolate",
  "Código roto porque falta un ;",
];

var onLoadPersonalInfo = () => {
  var myName = document.getElementById("personal_info_name");
  myName.innerHTML = personalInfo.name;
  var myAddress = document.getElementById("personal_info_address");
  myAddress.innerHTML = personalInfo.address;

  var myContact = document.getElementById("contact-list");
  contact.forEach((item) => {
    var anchor = document.createElement("a");
    anchor.href = item.url;
    var anchorImage = document.createElement("img");
    anchorImage.src = item.icon;
    anchorImage.classList.add("contact-list-item");
    anchor.appendChild(anchorImage);
    myContact.appendChild(anchor);
  });
  myContact.classList.add("list-no-decoration");

  var like = document.getElementById("like");
  var likeTitle = document.createElement("h4");
  likeTitle.classList.add("main__like-title");
  likeTitle.innerText = "Cosas que me encantan :)";
  like.appendChild(likeTitle);

  var likeList = document.getElementById("more-information-like");
  cosasGustan.forEach((like) => {
    var li = document.createElement("li");
    li.innerHTML = like;
    likeList.appendChild(li);
  });
  likeList.classList.add("list-no-decoration");

  var notlike = document.getElementById("notLike");
  var notLikeTitle = document.createElement("h4");
  notLikeTitle.classList.add("main__like-title");
  notLikeTitle.innerText = "Cosas que no me gustan :(";
  notlike.appendChild(notLikeTitle);

  var notLikeList = document.getElementById("more_information-not-like");
  cosasOdio.forEach((like) => {
    var li = document.createElement("li");
    li.innerHTML = like;
    notLikeList.appendChild(li);
  });
  notLikeList.classList.add("list-no-decoration");
};

export { onLoadPersonalInfo };
