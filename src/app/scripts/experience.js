var experience = [
  {
    nameJob: "Customer Success Engineer",
    companyName: "Worldsensing",
    date: "2019 a actualmente",
    moreInfo:
      "El departamento de Customer Success se encarga principalmente de la atención al cliente a través de la plataforma de tickets y, si es necesario, de llamada o ayuda sobre el terreno. Yo personalmente me encargo de la parte informática por lo que cualquier duda o problema que pueda surgir en la parte técnica del producto es mi tarea. Además de esto, también nos encargamos de la formación de los clientes en el uso de nuestros dispositivos, ya sea a nuevos clientes o sobre las nuevas tecnologías que hemos desarrollado. También nos encargamos de los manuales. Así, creamos y actualizamos los manuales de nuestros dispositivos para que luego nuestros clientes puedan ser más autónomos en el aprendizaje de su uso. Y si es necesario, ayudamos a otros departamentos en cualquier aspecto que lo requiera: pruebas de nuevos desarrollos, nuevos procesos, ventas...",
  },
  {
    nameJob: "Vendedora telefonía",
    companyName: "Vodafone",
    date: "2019",
    moreInfo:
      "Ventas, atención al cliente, cobro, mantenimiento de la tienda... Todo lo necesario para poder dar el mejor soporte al cliente esté o no en la tienda, asegurando una tienda en perfecto estado y buenos servicios. Además de enseñar a los nuevos compañeros y darles el apoyo necesario para que sean autónomos en el menor tiempo posible pero siempre asegurando un buen aprendizaje. También la creación de documentación sobre las herramientas utilizadas internamente para centralizar todo el conocimiento que estaba disperso entre los diferentes trabajadores. De manera que cualquiera pudiera acceder a esos conocimientos y ponerlos en práctica en su trabajo diario.",
  },
  {
    nameJob: "Vendedora tecnología FNAC",
    companyName: "FNAC",
    date: "2017 a 2019",
    moreInfo:
      "El objetivo principal era asesorar a los clientes en todas sus necesidades tecnológicas, la venta de tecnología y la atención al cliente después de la propia venta. A esto se añadía el mantenimiento de la zona designada: control de stock, pedidos, cobros, etiquetado, reasignación de zonas, incluso mejora del diseño. Todo lo necesario para mantener la tienda lo mejor posible para el cliente. Aparte de esto, después de un tiempo en la tienda, también se me asignó la formación en tienda de los nuevos empleados, enseñándoles cómo funciona todo, dándoles las herramientas necesarias para poder dar el mejor servicio posible de forma autosuficiente.",
  },
  {
    nameJob: "Becaria departamento informática",
    companyName: "Gestmusic",
    date: "2016 a 2017",
    moreInfo:
      "No había tareas fijas. Iba rotando de manera que pudiera familiarizarme con la mayor parte de las tareas del departamento, desde la introducción de datos en la base de datos hasta la toma de fotografías, pasando por las nuevas incorporaciones o la configuración de la página web.",
  },
];

var experienceWsDiv = document.getElementById("ws-container");
var experienceVfDiv = document.getElementById("vf-container");
var experienceFnacDiv = document.getElementById("fnac-container");
var experienceGmDiv = document.getElementById("gm-container");

var onlyShowWorldsensing = () => {
  experienceWsDiv.classList.remove("hide");
  experienceVfDiv.classList.add("hide");
  experienceFnacDiv.classList.add("hide");
  experienceGmDiv.classList.add("hide");
};

var onlyShowVodafone = () => {
  experienceWsDiv.classList.add("hide");
  experienceVfDiv.classList.remove("hide");
  experienceFnacDiv.classList.add("hide");
  experienceGmDiv.classList.add("hide");
};

var onlyShowFnac = () => {
  experienceWsDiv.classList.add("hide");
  experienceVfDiv.classList.add("hide");
  experienceFnacDiv.classList.remove("hide");
  experienceGmDiv.classList.add("hide");
};

var onlyShowGestmusic = () => {
  experienceWsDiv.classList.add("hide");
  experienceVfDiv.classList.add("hide");
  experienceFnacDiv.classList.add("hide");
  experienceGmDiv.classList.remove("hide");
};

var onLoadExperience = () => {
  var experienceWsList = document.getElementById("info-ws");
  var experienceVfList = document.getElementById("info-vf");
  var experienceFnacList = document.getElementById("info-fnac");
  var experienceGmList = document.getElementById("info-gm");
  experienceWsList.classList.add("list-no-decoration");
  experienceVfList.classList.add("list-no-decoration");
  experienceFnacList.classList.add("list-no-decoration");
  experienceGmList.classList.add("list-no-decoration");

  var liWs1 = document.createElement("li");
  liWs1.classList.add("main__info-li1");
  liWs1.innerText =
    experience[0].nameJob + " en el periodo de " + experience[0].date + ".";
  experienceWsList.appendChild(liWs1);
  var liWs2 = document.createElement("li");
  liWs2.classList.add("main__info-li2");
  liWs2.innerText = experience[0].moreInfo + ".";
  experienceWsList.appendChild(liWs2);

  var liVf1 = document.createElement("li");
  liVf1.classList.add("main__info-li1");
  liVf1.innerText =
    experience[1].nameJob + " en el periodo de " + experience[1].date + ".";
  experienceVfList.appendChild(liVf1);
  var liVf2 = document.createElement("li");
  liVf2.classList.add("main__info-li2");
  liVf2.innerText = experience[1].moreInfo + ".";
  experienceVfList.appendChild(liVf2);

  var liFnac1 = document.createElement("li");
  liFnac1.classList.add("main__info-li1");
  liFnac1.innerText =
    experience[2].nameJob + " en el periodo de " + experience[2].date + ".";
  experienceFnacList.appendChild(liFnac1);
  var liFnac2 = document.createElement("li");
  liFnac2.classList.add("main__info-li2");
  liFnac2.innerText = experience[2].moreInfo + ".";
  experienceFnacList.appendChild(liFnac2);

  var liGm1 = document.createElement("li");
  liGm1.classList.add("main__info-li1");
  liGm1.innerText =
    experience[3].nameJob + " en el periodo de " + experience[3].date + ".";
  experienceGmList.appendChild(liGm1);
  var liGm2 = document.createElement("li");
  liGm2.classList.add("main__info-li2");
  liGm2.innerText = experience[3].moreInfo + ".";
  experienceGmList.appendChild(liGm2);
};

export {
  onLoadExperience,
  onlyShowWorldsensing,
  onlyShowVodafone,
  onlyShowFnac,
  onlyShowGestmusic,
};
