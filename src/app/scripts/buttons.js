var mainPageSection = document.getElementById("section-main-page");
var personalInfoSection = document.getElementById("section-personal-info");
var experienceSection = document.getElementById("section-experience-page");
var knowledgeSection = document.getElementById("section-knowledge-page");
var curriculumSection = document.getElementById("section-curriculum-page");

var onlyShowMainPage = () => {
  mainPageSection.classList.remove("hide");
  personalInfoSection.classList.add("hide");
  knowledgeSection.classList.add("hide");
  experienceSection.classList.add("hide");
  curriculumSection.classList.add("hide");
};

var nextSectionMainPage = () => {
  mainPageSection.classList.add("hide");
  personalInfoSection.classList.remove("hide");
  timeoutID();
};

var onlyShowPersonalPage = () => {
  mainPageSection.classList.add("hide");
  personalInfoSection.classList.remove("hide");
  knowledgeSection.classList.add("hide");
  experienceSection.classList.add("hide");
  curriculumSection.classList.add("hide");
  timeoutID();
};

var nextSectionPersonalPage = () => {
  personalInfoSection.classList.add("hide");
  knowledgeSection.classList.remove("hide");
};
var onlyShowKnowledgelPage = () => {
  mainPageSection.classList.add("hide");
  personalInfoSection.classList.add("hide");
  knowledgeSection.classList.remove("hide");
  experienceSection.classList.add("hide");
  curriculumSection.classList.add("hide");
};

var nextSectionKnowledgePage = () => {
  knowledgeSection.classList.add("hide");
  experienceSection.classList.remove("hide");
};

var onlyShowExperiencePage = () => {
  mainPageSection.classList.add("hide");
  personalInfoSection.classList.add("hide");
  knowledgeSection.classList.add("hide");
  experienceSection.classList.remove("hide");
  curriculumSection.classList.add("hide");
};

var nextSectionExperiencePage = () => {
  experienceSection.classList.add("hide");
  curriculumSection.classList.remove("hide");
};

var onlyShowCurriculumPage = () => {
  mainPageSection.classList.add("hide");
  personalInfoSection.classList.add("hide");
  knowledgeSection.classList.add("hide");
  experienceSection.classList.add("hide");
  curriculumSection.classList.remove("hide");
};

var GoToPersonal = () => {
  curriculumSection.classList.add("hide");
  personalInfoSection.classList.remove("hide");
};

var timeoutID = () => {
  timeoutID = setTimeout(slowAlert, 5000);
};
function slowAlert() {
  alert("¡No tengas vergüenza! Contáctame y hablemos :)");
}

export {
  onlyShowMainPage,
  onlyShowPersonalPage,
  onlyShowKnowledgelPage,
  onlyShowExperiencePage,
  nextSectionMainPage,
  nextSectionPersonalPage,
  nextSectionKnowledgePage,
  nextSectionExperiencePage,
  onlyShowCurriculumPage,
  GoToPersonal,
};
