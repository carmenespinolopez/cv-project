var studies = [
  {
    name: "Upgrade hub",
    title: "Full Stack Developer",
    years: "2021",
    img: "/assets/upgradehub.jpg",
  },
  {
    name: "Escola del Treball",
    title: "GM: sistemas microinformáticos y redes",
    years: "2015-2017",
    img: "/assets/escoladeltreball.jpg",
  },
  {
    name: "Sant Josep Obrer",
    title: "Bachillerato Humanístico",
    years: "2012-2014",
    img: "/assets/santjosepobrer.jpg",
  },
];

var techKnowledge = [
  {
    name: "HTML 5",
    level: "Medio",
  },
  {
    name: "CSS",
    level: "Medio",
  },
  {
    name: "JavaScript",
    level: "Básico",
  },
];
var langKnowledge = [
  {
    name: "Inglés",
    level: "Alto",
  },
  {
    name: "Castellano",
    level: "Nativo",
  },
  {
    name: "Catalán",
    level: "Nativo",
  },
];
var softSkillsKnowledge = [
  {
    name: "Comunicación",
    level: "Alto",
  },
  {
    name: "Resolución",
    level: "Alto",
  },
  {
    name: "Trabajo en equipo",
    level: "Alto",
  },
];

var knowledgeImages = [
  "/assets/tech.jpg",
  "/assets/lang.jpeg",
  "/assets/softskills.png",
];

var onLoadKnowledge = () => {
  var studiesDiv = document.getElementById("studies");
  for (let i = 0; i < studies.length; i++) {
    if (i % 2 === 0) {
      console.log("Impar " + i);

      var divEvenContainer = document.createElement("div");
      divEvenContainer.classList.add("main__studies-div");
      studiesDiv.appendChild(divEvenContainer);

      var divEvenImg = document.createElement("div");
      divEvenImg.classList.add("main__studies-img-div");

      var imgEven = document.createElement("img");
      imgEven.classList.add("main__studies-img");
      imgEven.src = studies[i].img;
      divEvenImg.appendChild(imgEven);
      divEvenContainer.appendChild(divEvenImg);

      var divEvenInfo = document.createElement("div");
      divEvenInfo.classList.add("main__studies-info-div");
      divEvenContainer.appendChild(divEvenInfo);
      var pEven1 = document.createElement("p");
      pEven1.innerText = studies[i].title;
      pEven1.classList.add("font-weight-800");
      divEvenInfo.appendChild(pEven1);
      var pEven2 = document.createElement("p");
      pEven2.innerText = studies[i].name;
      divEvenInfo.appendChild(pEven2);
      var pEven3 = document.createElement("p");
      pEven3.innerText = studies[i].years;
      divEvenInfo.appendChild(pEven3);
    } else {
      var divEvenContainer = document.createElement("div");
      divEvenContainer.classList.add("main__studies-div");
      studiesDiv.appendChild(divEvenContainer);

      var divEvenInfo = document.createElement("div");
      divEvenInfo.classList.add("main__studies-info-div");
      divEvenContainer.appendChild(divEvenInfo);
      var pEven1 = document.createElement("p");
      pEven1.innerText = studies[i].title;
      pEven1.classList.add("font-weight-800");
      divEvenInfo.appendChild(pEven1);
      var pEven2 = document.createElement("p");
      pEven2.innerText = studies[i].name;
      divEvenInfo.appendChild(pEven2);
      var pEven3 = document.createElement("p");
      pEven3.innerText = studies[i].years;
      divEvenInfo.appendChild(pEven3);

      var divEvenImg = document.createElement("div");
      divEvenImg.classList.add("main__studies-img-div");

      var imgEven = document.createElement("img");
      imgEven.classList.add("main__studies-img");
      imgEven.src = studies[i].img;
      divEvenImg.appendChild(imgEven);
      divEvenContainer.appendChild(divEvenImg);
    }
  }
  var knowledgeDiv = document.getElementById("knowledge");
  for (let i = 0; i < 3; i++) {
    var knowledgeSpan = document.createElement("span");
    knowledgeSpan.classList.add("main__knowledge-span");
    knowledgeDiv.appendChild(knowledgeSpan);

    var knowledgeList = document.createElement("ul");
    knowledgeList.classList.add("list-no-decoration");
    knowledgeList.classList.add("center");
    knowledgeSpan.appendChild(knowledgeList);

    if (i === 0) {
      var techListTitle = document.createElement("h3");
      techListTitle.innerText = "Programación";
      knowledgeList.appendChild(techListTitle);
      for (let i = 0; i < techKnowledge.length; i++) {
        var knowledgeListItem = document.createElement("li");
        knowledgeListItem.classList.add("main__knowledge-list-item");
        knowledgeListItem.innerText =
          techKnowledge[i].name + " - " + techKnowledge[i].level;
        knowledgeList.appendChild(knowledgeListItem);
      }
      var knowImage = document.createElement("img");
      knowImage.src = knowledgeImages[i];
      knowImage.classList.add("main__knowledge-image");
      knowledgeList.appendChild(knowImage);
    } else if (i === 1) {
      var techLangTitle = document.createElement("h3");
      techLangTitle.innerText = "Idiomas";
      knowledgeList.appendChild(techLangTitle);
      for (let i = 0; i < langKnowledge.length; i++) {
        var knowledgeListItem = document.createElement("li");
        knowledgeListItem.classList.add("main__knowledge-list-item");
        knowledgeListItem.innerText =
          langKnowledge[i].name + " - " + langKnowledge[i].level;
        knowledgeList.appendChild(knowledgeListItem);
      }
      var knowImage = document.createElement("img");
      knowImage.src = knowledgeImages[i];
      knowImage.classList.add("main__knowledge-image");
      knowledgeList.appendChild(knowImage);
    } else if (i === 2) {
      var techSoftSkillsTitle = document.createElement("h3");
      techSoftSkillsTitle.innerText = "Soft skills";
      knowledgeList.appendChild(techSoftSkillsTitle);
      for (let i = 0; i < softSkillsKnowledge.length; i++) {
        var knowledgeListItem = document.createElement("li");
        knowledgeListItem.classList.add("main__knowledge-list-item");
        knowledgeListItem.innerText =
          softSkillsKnowledge[i].name + " - " + softSkillsKnowledge[i].level;
        knowledgeList.appendChild(knowledgeListItem);
      }
      var knowImage = document.createElement("img");
      knowImage.src = knowledgeImages[i];
      knowImage.classList.add("main__knowledge-image");
      knowledgeList.appendChild(knowImage);
    }
  }
};

export { onLoadKnowledge };
