# CV

## Usage 

After downloading/cloning the repository, go to the location where it is and install all the dependences listed on the package.json

```bash
npm install
```

Start the service to visualize the website:

```bash
npm run dev
```

Open the [link to localhost:4000](localhost:4000).

